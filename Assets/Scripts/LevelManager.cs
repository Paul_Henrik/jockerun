﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


public class LevelManager : MonoBehaviour
{
    public static bool isPlayerAlive = true;
    public static int MountainAmount;
	// Use this for initialization
	void Start ()
	{

	}

	// Update is called once per frame
	void Update ()
	{
        Debug.Log("Mountains on map: " + MountainAmount);
        playerAlive();
        quit();
    }

    void playerAlive()
    {
        if (!isPlayerAlive)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            MountainAmount = 0;
            isPlayerAlive = true;
        }
    }

    void quit()
    {
        if (Input.GetKey("escape"))
            Application.Quit();
    }

}