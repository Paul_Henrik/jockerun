﻿using UnityEngine;
using System.Collections;

public class ObjectMover : MonoBehaviour
{
    public float speed;
    public bool randomSpeed;
    public bool cloud;
    public bool mountain;
    private float rnd;
	// Use this for initialization
	void Start ()
	{
        if (randomSpeed)
        {
            speed = Random.Range(0.05f, 0.2f);
            if (speed < 0.1f && cloud)                   //For clouds
            {
                transform.Rotate(180, 0, 180);
            }
            
        }
        if (mountain)
        {
            Debug.Log("mountain Works");
            rnd = Random.Range(6, 10);              //For a more random scale on the mountains
            transform.localScale = new Vector3(rnd, rnd, rnd);
        }
    }

	// Update is called once per frame
	void Update ()
	{
        transform.position = new Vector2(transform.position.x - speed, transform.position.y);
	}
}