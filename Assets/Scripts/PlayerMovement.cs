﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour
{
    public float jumpHeight = 15f;
    private float jumpTimer = 0.17f;
    private float oldJumpTimer;

    public Transform groundCheck;
    public float groundCheckRadius;
    public LayerMask whatisGround;
    public bool grounded;
    private Rigidbody2D rb2d;
    public float speed;
    private float prevSpeed;
    public static bool blushingCloud; //If player touches a cloud he will get extra speed =)
    public Animator anim;

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        oldJumpTimer = jumpTimer;
        anim = GetComponentInChildren<Animator>();
        prevSpeed = speed;
        
    }

    void FixedUpdate()
    {
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatisGround); //Checks if player is standing on the ground. Also the surface objects needs to be in a layer called "ground"
    }

    // Update is called once per frame
    void Update()
    {
        jump();
        runFaster();
        death();
        if (grounded)
            anim.SetBool("Grounded", grounded);
    }

    void jump()
    {
        jumpTimer -= Time.deltaTime;
        if (jumpTimer <= 0)
        {
            if ((Input.GetKeyDown("w") || Input.GetKeyDown("space")) && grounded)
            {
                rb2d.velocity = new Vector2(rb2d.velocity.x, jumpHeight);
                jumpTimer = oldJumpTimer;
            }
        }
        /*if (transform.position.x <= -6)
            oldJumpTimer = 0;
        else
            oldJumpTimer = 0.21f;*/
    }

    void runFaster()
    {
        if (transform.position.x < 0)
        {
            
            rb2d.velocity = new Vector2(speed, rb2d.velocity.y);
            if (blushingCloud)
            {

                StartCoroutine("coblushingCloud");
                blushingCloud = false;
            }
        }
    }

    public IEnumerator coblushingCloud()
    {
        float _speedAdd = 1f;

        Debug.Log(speed);
        speed =+ _speedAdd;
        rb2d.velocity = new Vector2(speed, rb2d.velocity.y);
        yield return new WaitForSeconds(2);
        speed = prevSpeed;
    }

    void death()
    {
        if ((transform.position.y < -4) || (transform.position.x < -9))
        {
            LevelManager.isPlayerAlive = false;
        }
    }
}