﻿using UnityEngine;
using System.Collections;
public class Spawner : MonoBehaviour
{
    private int rnd;                    //Random number
    public int randomRange;             //Stupid name for number for adding higher blocks to jump
    public int height;                  //Height blocks will go to
    public int plusHeight;
    public int maxHeight;               //Maximum Height
    private float spawnTimer = 1f; // NEW
    public float _spawnTimer = 1f;
    private int stepBasedDifficulty;    //Adds more height or something every 100 steps the player take
    public GameObject[] spawnPoint;     //GameObject spawnPosition;
    public GameObject[] spawnObject;    //Gameobject Type Dirt, Grass etc.

    void Start()
    {
        maxHeight = 2;
    }


    void Update()
    {
        spawnCircle();
        
    }
    public void spawnCircle()
    {

        {
            spawnTimer -= Time.deltaTime;
            if (spawnTimer < 0.0f)
            {
                spawnTimer = _spawnTimer;
                spawn();
            }
        }
    }

    public void spawn()
    {
        if (ScoreManager.score >= 500)
        {
            if (height <= 1)
                rnd = Random.Range(0, 2) + randomRange;
            else
                rnd = Random.Range(-2, 2) + randomRange;
        }
        else
            rnd = Random.Range(-2, 2) + randomRange;

        height += rnd;
        if (height >= maxHeight)
            height = maxHeight;
        if (height <= 0)
            height = 0;
        checkActive();
        ScoreManager.AddPoints(1);
        stepBasedDifficulty++;
        if (stepBasedDifficulty >= 100 && ScoreManager.score <= 400)
        {
            addHeight(1);
            stepBasedDifficulty = 0;
        }

    }

    public void checkActive()
    {
        
        for (int i = 0; i < spawnPoint.Length; i++)
        {
            if (i < height)
            {
                spawnPoint[i].gameObject.SetActive(true);
                Instantiate(spawnObject[0], spawnPoint[i].transform.position, Quaternion.identity);
            }
            else if (i == height)
                Instantiate(spawnObject[1], spawnPoint[i].transform.position, Quaternion.identity);
            else
            {
                spawnPoint[i].gameObject.SetActive(false);
            }

        }
    }

    public void addHeight(int additioHeight)
    {
        maxHeight += additioHeight;
    }
}

