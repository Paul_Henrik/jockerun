﻿using UnityEngine;
using System.Collections;

public class cloudSpawn : MonoBehaviour
{
    public GameObject[] spawnPoint;
    public GameObject[] cloud;
    private float countDown;
    public float changeCountDown = 5f;
    public int rnd;

	// Use this for initialization
	void Start ()
	{
        countDown = changeCountDown;
	}

	// Update is called once per frame
	void Update ()
	{
        checkActive();
	}

    void checkActive()
    {
        countDown -= Time.deltaTime;
        if (countDown < 0)
        {
            countDown = changeCountDown;
            rnd = Random.Range(0, spawnPoint.Length);
            spawnPoint[rnd].gameObject.SetActive(true);

            for (int i = 0; i < spawnPoint.Length; i++)
            {
                if (spawnPoint[i].gameObject.activeInHierarchy == true)
                    {
                    Instantiate(cloud[0], spawnPoint[i].transform.position, Quaternion.identity);
                    spawnPoint[i].SetActive(false);
                    }
            }
            
        }
    }
}